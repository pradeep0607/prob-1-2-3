const input = ['dog', 'chicken', 'cat', 'dog', 'chicken', 'chicken', 'rabbit'];

function duplicate_in_list(array){      
const count = {}
const result = []

array.forEach(item => {
    if (count[item]) {
       count[item] +=1
       return
    }
    count[item] = 1
})

for (let prop in count){
    if (count[prop] >=2){
        result.push(prop)
    }
}

console.log(count)
return result;

}

duplicate_in_list(input)